$(function () {
	var $body = $('body');

	$('[data-toggle="tooltip"]').tooltip();

	$body.on('click', '.js-fancybox-open', function () {
		var $this = $(this),
			id = $this.attr('href') || $this.data('href');

		$.fancybox($(id), {
			padding: 55,
			maxWidth: 1000,
			scrolling: 'no'
		});
		return false;
	});

	$body.on('submit', '.js-feedback-form', function () {
		var $form = $(this);

		$.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			data: $form.serialize(),
			success: function (data) {
				if (+data) {
					$form.fadeOut().before('<div class="alert alert-success">Отправлено</div>');
				} else {
					$form.before('<div class="alert alert-danger">Ошибка</div>');
				}
			}
		});
		return false;
	});
});