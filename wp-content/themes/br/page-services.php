<?php

get_header();

inc('inc/two-columns-page', [

]);

inc('inc/benefits');

inc('inc/portfolio', [
	'title'        => 'Наши работы в сфере разработки сайтов',
	'categoryName' => 'web-dev',
]);

dynamic_sidebar('bottom-1');

get_footer();