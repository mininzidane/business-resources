<div class="col-sm-4">
	<div class="portfolio__item">
		<a href="<?php the_permalink() ?>">
			<?php
			// Post thumbnail.
			the_post_thumbnail();
			?>
			<span class="portfolio__item-hover"><b><?php the_title() ?></b></span>
		</a>
	</div>
</div>