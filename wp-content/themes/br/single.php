<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header();
$postId = get_the_ID();
?>

	<div class="post-y-2">
		<div class="container">
			<?php the_breadcrumbs() ?>

			<div class="row">
				<div class="col-sm-3">
					<?php wp_nav_menu([
						'menu_class'     => 'left-menu',
						'theme_location' => 'left-menu',
						'container'      => '',
					]); ?>

					<?php inc('inc/sidebar-accounting'); ?>
				</div>
				<div class="col-sm-9">
					<?php the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<div><?= get_post_meta($postId, 'description', true) ?></div>
					<?php $link = get_post_meta($postId, 'link', true); ?>
					<p class="post-y-1"><a href="<?= $link ?>" target="_blank" class="link"><?= $link ?></a></p>

					<div class="post-y-2">
						<div class="bg-notebook-folio bg-notebook-folio_centered">
							<?php the_post_thumbnail() ?>
						</div>
					</div>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>