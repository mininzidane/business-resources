<?php

/**
 * Register widget area.
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function br_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'br' ),
		'id'            => 'bottom-1',
		'description'   => __( 'Feedback form', 'br' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'br_widgets_init' );

function br_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyfifteen
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain('br');
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support('title-tag');
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(825, 510, true);

	register_nav_menus([
		'primary'              => __('Top Menu', 'br'),
		'left-menu'            => __('Left menu', 'br'),
		'left-menu-accounting' => __('Left menu accounting', 'br'),
	]);
}

add_action( 'after_setup_theme', 'br_setup' );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
//function twentyfifteen_search_form_modify( $html ) {
//	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
//}
//add_filter( 'get_search_form', 'twentyfifteen_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
//require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
//require get_template_directory() . '/inc/customizer.php';

function the_breadcrumbs() {
	echo '<ul class="breadcrumbs">';
	if (!is_home()) {
		echo '<li><a href="';
		echo get_option('home');
		echo '">';
		echo 'Главная';
		echo "</a></li> ";
		if (is_category() || is_single()) {
			echo '<li>';
			the_category(' </li><li> ');
			if (is_single()) {
				echo "</li><li>";
				the_title();
				echo '</li>';
			}
		} elseif (is_page()) {
			echo '<li>';
			echo the_title();
			echo '</li>';
		}
	} elseif (is_tag()) {
		single_tag_title();
	} elseif (is_day()) {
		echo "<li>Archive for ";
		the_time('F jS, Y');
		echo '</li>';
	} elseif (is_month()) {
		echo "<li>Archive for ";
		the_time('F, Y');
		echo '</li>';
	} elseif (is_year()) {
		echo "<li>Archive for ";
		the_time('Y');
		echo '</li>';
	} elseif (is_author()) {
		echo "<li>Author Archive";
		echo '</li>';
	} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
		echo "<li>Blog Archives";
		echo '</li>';
	} elseif (is_search()) {
		echo "<li>Search Results";
		echo '</li>';
	}
	echo '</ul>';
}

/**
 * Include file in theme with passed params
 *
 * @param string $fileName relative path from theme
 * @param array  $params   Params to pass to view
 */
function inc($fileName, $params = []) {
	extract($params);
	include "{$fileName}.php";
}

function br_template($template) {

	if (is_page('web-dev')) {
		if ($newTemplate = locate_template(['page-web-dev.php'])) {
			return $newTemplate;
		}
	}

	if (is_page('seo')) {
		if ($newTemplate = locate_template(['page-seo.php'])) {
			return $newTemplate;
		}
	}

	if (is_page('ads')) {
		if ($newTemplate = locate_template(['page-ads.php'])) {
			return $newTemplate;
		}
	}

	if (is_page('accounting')) {
		if ($newTemplate = locate_template(['page-accounting.php'])) {
			return $newTemplate;
		}
	}

	if (is_page('services')) {
		if ($newTemplate = locate_template(['page-services.php'])) {
			return $newTemplate;
		}
	}

	if (is_page('portfolio')) {
		if ($newTemplate = locate_template(['page-portfolio.php'])) {
			return $newTemplate;
		}
	}

	if (is_page('integration')) {
		if ($newTemplate = locate_template(['page-integration.php'])) {
			return $newTemplate;
		}
	}

	return $template;
}
add_filter('template_include', 'br_template');

function sendEmail() {
	$message   = '';
	$from      = 'no-reply@br.ru'; // change
	$formNames = ['feedback']; // change for yours form namespaces

	$formData = [];
	foreach ($formNames as $formName) {
		if (isset($_POST[ $formName ]) && is_array($_POST[ $formName ])) {
			$formData = $_POST[ $formName ];
			break;
		}
		if (isset($_GET[ $formName ]) && is_array($_GET[ $formName ])) {
			$formData = $_GET[ $formName ];
			break;
		}
	}

	foreach (array_keys($formData) as $key) {
		if ($key != 'subject' && ! empty($formData[ $key ])) {
			$translated_key = $key;
			switch ($key) {
				case 'name':
					$translated_key = 'ФИО контактного лица';
					break;
				case 'company':
					$translated_key = 'Название организации';
					break;
				case 'phone':
					$translated_key = 'Номер телефона';
					break;
				case 'city':
					$translated_key = 'Город';
					break;
				case 'email':
					$translated_key = 'Адрес электронной почты';
					break;
				case 'comment':
					$translated_key = 'Ваш вопрос';
					break;
			}
			$message .= $translated_key . ': ' . $formData[ $key ] . "\r\n";
		}
		//	if ($key == 'email' && !empty($formData[$key])) {
		//		$from = $_GET[$key];
		//	}
	}

	$message = iconv('UTF-8', 'windows-1251', $message);
	$to      = 'mininzidane@mail.ru';
	$subject = isset($formData['subject'])? $formData['subject']: '';
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=windows-1251' . "\r\n";
	$headers .= 'From: ' . $from . "\r\n";
	$headers .= 'Reply-To: ' . $from;
	echo (int) mail($to, $subject, $message, $headers);
	die;
}

add_action('wp_ajax_sendEmail', 'sendEmail');