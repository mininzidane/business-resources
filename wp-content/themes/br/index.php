<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="promo">
	<div class="container">
		<div class="text-center">
			<a href="/" class="logo">
				Деловые ресурсы
				<span>Ресурсы для вашего дела</span>
			</a>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="post-y-4">
					<a href="/services/" class="promo__link">Комплексный интернет-маркетинг</a>
				</div>
				<img class="mw-100 promo__banner" src="images/marketing.png" alt="">
			</div>
			<div class="col-sm-6">
				<div class="post-y-4">
					<a href="/accounting/" class="promo__link">Бухгалтерские услуги</a>
				</div>
				<img class="mw-100 promo__banner promo__banner_accounting" src="images/accounting.png" alt="">
			</div>
		</div>
	</div>
	<div class="promo__mouse"></div>
</div>
<div class="container pre-y-4 post-y-4">
	<h1 class="text-center">О компании</h1>
	<p class="fs-19 text-center">Компания &laquo;Деловые ресурсы&raquo; создана для клиентов, которые хотят
		сосредоточиться на&nbsp;эффективном управлении и&nbsp;обеспечении процветания своей компании. Услуги,
		оказываемые нашей компанией (<a href="/accounting/">Бухгалтерское сопровождение</a> и&nbsp;<a href="/services/">комплексный
			<nobr>интернет-маркетинг</nobr>
		</a>) являются необходимыми ресурсами для Вашего дела, нужны,
		чтобы Вы&nbsp;могли сосредоточиться на&nbsp;Вашем основном направлении бизнеса.
	</p>
</div>

<?php inc('inc/benefits') ?>

<?php inc('inc/portfolio', [
	'title'        => 'Наши работы в сфере разработки сайтов',
	'categoryName' => 'web-dev',
]); ?>

<?php dynamic_sidebar( 'bottom-1' ); ?>

<?php get_footer(); ?>