<?php

get_header();

inc('inc/two-columns-page', [

]);
?>
	<div class="dev-advantage">
		<div class="container">
			<h2 class="white text-center post-y-1">Преимущества разработки</h2>
			<div class="row">
				<div class="col-sm-4">
					<ul class="num-list">
						<li class="active"><a href="#markup" data-toggle="tab">Кроссбраузерная верстка</a></li>
						<li><a href="#design" data-toggle="tab">Уникальный дизайн</a></li>
						<li><a href="#manager" data-toggle="tab">Выделенный менеджер проекта</a></li>
						<li><a href="#test" data-toggle="tab">Тестовая площадка сайта</a></li>
						<li><a href="#hosting" data-toggle="tab">Перенос сайта на хостинг</a></li>
						<li><a href="#varanty" data-toggle="tab">12 месяцев гарантии на веб-сайт</a></li>
						<li><a href="#maintenance" data-toggle="tab">Сопровождение сайта</a></li>
						<li><a href="#approach" data-toggle="tab">Комплексный подход</a></li>
					</ul>
				</div>
				<div class="col-sm-8">
					<div class="tab-content white fs-13">
						<div class="tab-pane active" id="markup">Во время всего процесса разработки сайта вам будет
							помогать профессионал в области управления проектами. Вы сможете задавать интересующие
							вопросы, получать консультации и помощь в постановке задач и планировании проекта
						</div>
						<div class="tab-pane" id="design">Design</div>
						<div class="tab-pane" id="manager">Manager</div>
						<div class="tab-pane" id="test">Text</div>
						<div class="tab-pane" id="hosting">Text</div>
						<div class="tab-pane" id="varanty">Text</div>
						<div class="tab-pane" id="maintenance">Text</div>
						<div class="tab-pane" id="approach">Text</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php /*<div class="dev-advantage">
		<div class="container">
			<h2 class="white text-center post-y-1">Преимущества разработки</h2>
			<div class="row">
				<div class="col-sm-3">
					<div class="dev-advantage__item" data-placement="bottom" data-toggle="tooltip"
						 title="Во время всего процесса разработки сайта вам будет помогать профессионал в области управления проектами. Вы сможете задавать интересующие вопросы, получать консультации и помощь в постановке задач и планировании проекта">
						Кроссбраузерная верстка
					</div>
				</div>
				<div class="col-sm-3">
					<div class="dev-advantage__item">
						Уникальный дизайн
					</div>
				</div>
				<div class="col-sm-3">
					<div class="dev-advantage__item">
						Выделенный менеджер проекта
					</div>
				</div>
				<div class="col-sm-3">
					<div class="dev-advantage__item">
						Тестовая площадка сайта
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					<div class="dev-advantage__item">
						Перенос сайта на хостинг
					</div>
				</div>
				<div class="col-sm-3">
					<div class="dev-advantage__item">
						12 месяцев гарантии на веб-сайт
					</div>
				</div>
				<div class="col-sm-3">
					<div class="dev-advantage__item">
						Сопровождение сайта
					</div>
				</div>
				<div class="col-sm-3">
					<div class="dev-advantage__item">
						Комплексный подход
					</div>
				</div>
			</div>
		</div>
	</div>*/ ?>

<?php
inc('inc/portfolio', [
	'title'        => 'Наши работы в сфере разработки сайтов',
	'categoryName' => 'web-dev',
]);

dynamic_sidebar('bottom-1');

get_footer();