<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/js/fancybox/jquery.fancybox.css">
	<link rel="stylesheet" href="/css/main.css">
</head>
<body <?php body_class(); ?>>
<?php if (!is_front_page()) { //  && is_home() ?>
	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<a href="/" class="logo">
						Деловые ресурсы
						<span>Ресурсы для Вашего дела</span>
					</a>
				</div>
				<div class="col-sm-4">
					<?php wp_nav_menu([
						'menu_class'     => 'top-menu',
						'theme_location' => 'primary',
						'container'      => '',
					]); ?>
				</div>
				<div class="col-sm-2">
					<a href="#submit-form" type="submit" class="btn btn-primary button-send">Отправить заявку</a>
				</div>
			</div>
		</div>
	</header>
<?php } ?>