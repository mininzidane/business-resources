<?php

get_header();
$portfolioSize = 8;
?>

	<div class="pre-y-2 post-y-2">
		<div class="container">
			<?php the_breadcrumbs() ?>

			<div class="row">
				<div class="col-sm-3">
					<?php wp_nav_menu([
						'menu_class'     => 'left-menu',
						'theme_location' => 'left-menu',
						'container'      => '',
					]); ?>

					<?php inc('inc/sidebar-accounting'); ?>
				</div>
				<div class="col-sm-9">
					<h2><?php the_title() ?></h2>
					<?php
					the_post();
					the_content();

					/** @var WP_Post[] $posts */
					$posts = get_posts([
						'numberposts' => $portfolioSize,
					]);
					foreach ($posts as $i => $post) { ?>
						<div class="works-vertical">
							<h3><?= $post->post_title ?></h3>
							<div class="row">
								<div class="col-sm-12 col-md-9">
									<div class="bg-notebook-folio">
										<?= get_the_post_thumbnail($post) ?>
									</div>
								</div>
								<div class="col-sm-12 col-md-3">
									<a href="<?= get_the_permalink($post) ?>" class="btn button">Подробнее</a>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php
					/** @var WP_Post[] $posts */
					$posts = get_posts([
						'numberposts' => 6,
						'offset'      => $portfolioSize,
					]); ?>
					<?php if ($count = count($posts)) { ?>
						<h3 class="post-y-1">Другие наши работы</h3>
					<?php } ?>
					<div class="row">
						<?php foreach ($posts as $i => $post) { ?>
							<div class="col-sm-4">
								<div class="frame-browser-small">
									<?= get_the_post_thumbnail($post) ?>
								</div>
								<p><a href="<?= get_the_permalink($post) ?>" class="link"><?= $post->post_title ?></a>
								</p>
							</div>
							<?php if (++$i % 3 == 0 && $i < $count) { ?>
					</div>
					<div class="row">
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();