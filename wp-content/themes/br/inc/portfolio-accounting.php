<?php
/**
 * @var string $title
 * @var string $categoryName Post category name
 */
?>
<div class="works">
	<h2 class="text-center"><?= isset($title)? $title: 'Наши работы' ?></h2>
	<ul class="works__list">
		<?php
		/** @var WP_Post[] $posts */
		$posts = get_posts([
			'category_name' => 'accounting',
			'numberposts' => 20,
		]);

		foreach ($posts as $post) { ?>
			<li><a href="<?= get_the_permalink($post) ?>"><?= $post->post_title ?></a></li>
		<?php } ?>
	</ul>
</div>