<div class="post-y-2">
	<div class="container">
		<?php the_breadcrumbs() ?>

		<div class="row">
			<div class="col-sm-3">
				<?php wp_nav_menu([
					'menu_class'     => 'left-menu',
					'theme_location' => 'left-menu',
					'container'      => '',
				]); ?>

				<?php inc('inc/sidebar-accounting'); ?>
			</div>
			<div class="col-sm-9">
				<?php
				the_post();
				the_content();
				?>
			</div>
		</div>
	</div>
</div>