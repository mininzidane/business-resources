<div class="pre-y-2 post-y-2">
	<div class="container">
		<?php the_breadcrumbs() ?>

		<div class="row">
			<div class="col-sm-3">
				<?php wp_nav_menu([
					'menu_class'     => 'left-menu',
					'theme_location' => 'left-menu-accounting',
					'container'      => '',
				]); ?>

				<div class="sidebar-image hidden-xs">
					<img src="/images/internet-marketing.png" alt="">
					<a href="/services/" class="black-link">Комплексный интернет-маркетинг</a>
				</div>
			</div>
			<div class="col-sm-9">
				<?php
				the_post();
				the_content();
				?>
			</div>
		</div>
	</div>
</div>