<div class="benefits">
	<div class="benefits__triangle"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 benefits__item"><i class="benefits__icon"></i>
				<h4 class="benefits__title">Опытные специалисты</h4>
				<div class="benefits__desc">Все предоставляемые услуги выполняются строго профессионалами с&nbsp;опытом
					работы более 10 лет. Никаких &laquo;недорогих&raquo; специалистов!
				</div>
			</div>
			<div class="col-sm-3 benefits__item"><i class="benefits__icon benefits__icon_2"></i>
				<h4 class="benefits__title">Опыт работы
					более 5 лет</h4>
				<div class="benefits__desc">Компания успешно работает с&nbsp;2011&nbsp;г.</div>
			</div>
			<div class="col-sm-3 benefits__item"><i class="benefits__icon benefits__icon_3"></i>
				<h4 class="benefits__title">
					Отсутствие &laquo;провальных&raquo; проектов</h4>
				<div class="benefits__desc">Мы&nbsp;просто грамотно и&nbsp;надежно делаем свое дело и&nbsp;не&nbsp;вводим
					клиента в&nbsp;заблуждение
				</div>
			</div>
			<div class="col-sm-3 benefits__item"><i class="benefits__icon benefits__icon_4"></i>
				<h4 class="benefits__title">Комплексный
					подход</h4>
				<div class="benefits__desc">Работая комплексно, мы&nbsp;обеспечиваем успешную работу целых
					вспомогательных
					структур Вашей компании
				</div>
			</div>
		</div>
	</div>
</div>