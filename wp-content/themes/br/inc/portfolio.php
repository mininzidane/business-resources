<?php
/**
 * @var string $title
 * @var string $categoryName Post category name
 */
?>
<div class="portfolio">
	<div class="container">
		<h2 class="text-center post-y-1"><?= $title ?></h2>
		<div class="row">
			<?php
			/** @var WP_Post[] $posts */
			$posts = get_posts([
				'category_name' => $categoryName,
				'numberposts'   => 3,
			]);
			$count = count($posts);
			foreach ($posts as $i => $post) { ?>
			<div class="col-sm-4">
				<div class="portfolio__item">
					<a href="<?= get_the_permalink($post) ?>">
						<?php if ($image = get_the_post_thumbnail($post)) { ?>
							<?= $image ?>
							<span class="portfolio__item-hover"><b><?= $post->post_title ?></b></span>
						<?php } else { ?>
							<?= $post->post_title ?>
						<?php } ?>
					</a>
				</div>
			</div>
			<?php if (++$i % 3 == 0 && $i < $count) { ?>
		</div>
		<div class="row">
			<?php } ?>
			<?php } ?>
		</div>

		<div class="text-center">
			<a href="/portfolio/" class="button-green">Все работы</a>
		</div>
	</div>
</div>