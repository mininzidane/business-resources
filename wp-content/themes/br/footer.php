<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<div class="footer__logo">Деловые ресурсы</div>
			</div>
			<div class="col-sm-6">
				<p>© Lorem ipsum — dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
				laoreet dolore magna aliquam erat.</p>
			</div>
			<div class="col-sm-2">
				<p>Телефон: <br>
				+ 7 (123) 456 76 89</p>
			</div>
			<div class="col-sm-2">
				<p>E-mail: <br>
				Mail@mail.ru</p>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<script
		src="https://code.jquery.com/jquery-2.2.4.min.js"
		integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
		crossorigin="anonymous"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="/js/main.js"></script>
</body>
</html>