<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'business-resources');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '28051989');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|q/[dCn+1-9y?7#Z]t8=i:9bf_=`4k`BIT4VI2FzZ9Eex;Efi:D@H,@>:VGP8>0x');
define('SECURE_AUTH_KEY',  '>!C?h/$=T{hdwB-{T%dS^*p0z7p4X ypq[=<O%KyAD~3R!{fZ%387K9vMke QNP$');
define('LOGGED_IN_KEY',    'wD7 G/1j3v, /3U@X]+P NB;[aVZ$O=Vza^HrP{4)^}L1{|rkP4Lde%hB<}Jc6k/');
define('NONCE_KEY',        'M?CCS/9[wDdb}Jz5^}W2sV#^K=g:n9^_F:~^S^dQ.hB8[6|Zt9;-DBWj++AaQv6M');
define('AUTH_SALT',        'q|tJU54!|-Z)ZMI&v!+Rh;cBI{%>r01?@ng368qIa9T7&wd;aMwB)%.T*ClwscK~');
define('SECURE_AUTH_SALT', '~tBJ/)a!K$km!*rsImNvAOJWCRLtH[>;m,78H-<-j DhlT0=MRtOescr&;iu#&(.');
define('LOGGED_IN_SALT',   'YXfST6/>,3YxAp2k)C<D477KHeJnuSruj[EG?Dou?3$s%-{lF9RnL;~u8-BLCVr&');
define('NONCE_SALT',       'VkW+p;Sq`iYx1%&jin4lKJg%#Z Ff*l7mvB}rD:o-*<}@g^dp*T/!E(i]<eTigUP');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
